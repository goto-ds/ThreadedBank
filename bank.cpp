#include <iomanip>
#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <algorithm>

#include<thread>
#include<mutex>
#include <ctime>
#include <chrono>

using namespace std;


const int N_OF_PRINTERS = 8;
const int N_OF_THREADS = 10;


class Semaphore {
  int n;
  int cur_locks = 0;
  mutex locking_mutex;
  mutex lock;

public:
  Semaphore(int n_) {
    n = n_;
  }

  void getLock() {
    locking_mutex.lock();
    if (cur_locks < n) {
      cur_locks += 1;
      if (cur_locks == n) {
        lock.lock();
      }
    } else {
      locking_mutex.unlock();
      lock.lock();
      locking_mutex.lock();
      cur_locks += 1;
    }
    locking_mutex.unlock();
  }

  void releaseLock() {
    if (cur_locks == n) {
      lock.unlock();
    }
    locking_mutex.lock();
    cur_locks = max(cur_locks - 1, 0);
    locking_mutex.unlock();
  }
};


class BankManager {
	float balance = 0;
public:
	void addTaxes(int client_balance){
		//this_thread::sleep_for(0.1s);
		balance += client_balance*0.1;
	}
};

class Report {};

class Account {
	int balance = 0;
public:
	Report createReport(int balance){
		this_thread::sleep_for(0.1s);
        Report r;
        return r;
	}

	int getBalance(){ // get balance
        return balance;
	}

	void update(int amount){ // update balance with amount
		if(balance + amount < 0){
			cerr << "Can not do transaction " << amount << "\n";
		} else {
			balance += amount;
		}
	}
};


Account client;
BankManager bank_master;

int number_of_operations = 0;
int current_index = 0;

vector<thread> threads;
thread procent_thread;

mutex mutex_update;
mutex mutex_reader;

Semaphore semaphore_printer(N_OF_PRINTERS);

void printReport(Report r) {
  this_thread::sleep_for(0.1s);
}

void process_operations(){
  while(true){ //cycle over data. DON'T FORGET TO MAKE A REPORT
  	int a;
    mutex_reader.lock();
    if(current_index >= number_of_operations) {
      mutex_reader.unlock();
      break;
    }
	cin >> a;
	current_index++;
    mutex_reader.unlock();

    mutex_update.lock();
	client.update(a);
	cout << client.getBalance() << '\n';
    int tmp = client.getBalance();
    mutex_update.unlock();

    Report report = client.createReport(tmp);

    semaphore_printer.getLock();
    printReport(report);
    semaphore_printer.releaseLock();
  }
}

void addProcents() {
  while(1) {
    mutex_update.lock();
    client.update(client.getBalance() * 0.1);
    mutex_update.unlock();
    this_thread::sleep_for(0.1s);
  }
}

void start_bank(){ //
  cin >> number_of_operations;  
  procent_thread = thread(addProcents);
  for(int i = 0; i < N_OF_THREADS; ++i) {
    threads.push_back(thread(process_operations));
  }
  for(int i = 0; i < N_OF_THREADS; ++i) {
    threads[i].join();
  }
  cout << "Resulting balance: " << client.getBalance() << endl;
  procent_thread.detach();
}
int main() {
	freopen("input.txt", "r", stdin);

   chrono::steady_clock sc;   // create an object of `steady_clock` class
   auto start = sc.now();     // start timer

   start_bank();
   auto end = sc.now();       // end timer (starting & ending is done by measuring the time at the moment the process started & ended respectively)
   auto time_span = static_cast<chrono::duration<double>>(end - start);   // measure time span between start & end
   cerr<<"Operation took: "<<time_span.count()<<" seconds !!!";

  return 0;
}
//vim: set softtabstop=0 noexpandtab
